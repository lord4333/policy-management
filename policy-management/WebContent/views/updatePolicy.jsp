<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Policy Details</title>
</head>
<body>
<h1>Update Policy</h1>
<a href="index.jsp">Home</a><br/><br/>
<s:form action="update" method="post" modelAttribute="u">
Policy Name<s:input path="policyName" autocomplete="off"></s:input><br/>
Policy Type<s:input path="policyType" autocomplete="off"></s:input><br/>
Duration<s:input path="duration" autocomplete="off"></s:input>
Amount<s:input path="amount" autocomplete="off"></s:input>
<s:button>Update</s:button>
</s:form>
</body>
</html>