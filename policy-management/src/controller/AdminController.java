package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dao.AdminDAO;
import dao.AdminDAOImpl;
import model.Admin;


@Controller
public class AdminController {
	AdminDAO dao= new AdminDAOImpl();
	@RequestMapping("adminRegister")
	public ModelAndView adminRegister() {
	return new ModelAndView("adminRegister","a",new Admin());
	}
	@RequestMapping("adminAdd")
	public ModelAndView add(@ModelAttribute("a") Admin a) {
		dao.insertAdmin(a);
		return new ModelAndView("display");
	}
@RequestMapping("adminLogin")
public ModelAndView adminLogin()
{
	return new ModelAndView("adminLogin","a",new Admin());
}
    @RequestMapping("adminValidate")
    public ModelAndView adminValidate(@ModelAttribute("a") Admin admin)
{
    	boolean isValid = dao.adminAuthentication(admin);
    	if(isValid) {
    	return new ModelAndView("policyRegister", "v", "Login Successfully");
    	} else {
    		return new ModelAndView("adminLogin", "v", "Check your Credentials");
    	}
}
}
