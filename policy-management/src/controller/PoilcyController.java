package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dao.PolicyDAO;
import dao.PolicyDAOImpl;
import model.Policy;

@Controller
public class PoilcyController {
	PolicyDAO dao=new PolicyDAOImpl();
	
	@RequestMapping("addPolicy")
	public ModelAndView addPolicy() {
	return new ModelAndView("addPolicy","p",new Policy());
	}
	
	@RequestMapping("add")
	public ModelAndView add(@ModelAttribute("p") Policy p) {
		dao.insertPolicy(p);
		return new ModelAndView("display");
	}
	
	@RequestMapping("updatePolicy")
	public ModelAndView update() {
		return new ModelAndView("updatePolicy","u",new Policy());
		
	}
	@RequestMapping("update")
	public ModelAndView update(@ModelAttribute("u") Policy policy) {
		dao.updatePolicy(Policy);
		List<Policy> list=dao.view();
		return new ModelAndView("viewPolicy","u",list);
	}

}
