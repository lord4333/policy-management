package dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import model.Policy;

public class PolicyDAOImpl implements PolicyDAO {
	
	
	public void insertPolicy(Policy policy) {
		Session session=new Configuration().configure("admincfg.xml").buildSessionFactory().getCurrentSession();
		
		Transaction tx=session.beginTransaction();
		session.save(policy);
		tx.commit();
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Policy> view() {
		Session session=new Configuration().configure("admincfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx=session.beginTransaction();
		List<Policy> list=new ArrayList<Policy>();
		Query q=session.createQuery("from Pilots");
		List<Pilots> l=q.list();
		for(Policy policy:l) {
			list.add(policy);
		}
		
		return list;
	}

	@Override
	public void updatePilots(Policy policy) {
		Session session=new Configuration().configure("admincfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx=session.beginTransaction();
		Query q=session.createQuery("update Policy set policyName=:policyName,policyType=:policyType,duration=:duration,amount=:amount where pid=:pid");
		q.setParameter("policyName",policy.getPolicyName());
		q.setParameter("policyType",policy.getPolicyType());
		q.setParameter("duration",policy.getDuration());
		q.setParameter("amount",policy.getAmount());
		q.setParameter("id",policy.getPid());
		q.executeUpdate();
		session.save(policy);
		
		
	}

}
