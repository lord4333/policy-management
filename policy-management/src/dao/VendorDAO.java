package dao;

import model.Vendor;

public interface VendorDAO {
	public boolean vendorAuthentication(Vendor vendor);
	public void insertVendor (Vendor vendor);
}