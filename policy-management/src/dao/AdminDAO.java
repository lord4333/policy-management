package dao;

import model.Admin;

public interface AdminDAO {
	public boolean adminAuthentication(Admin admin);
	public void insertAdmin (Admin admin);
}