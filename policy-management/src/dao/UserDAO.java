package dao;

import model.User;

public interface UserDAO {
	public boolean userAuthentication(User user);
	public void insertUser(User user);
}